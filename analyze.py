import pandas as pd
import tkinter
import matplotlib.pyplot as plt
from fpdf import FPDF

df = pd.read_csv('15CS.csv')

sub_code = '15CS53'
sub_internal = sub_code + "_internal"
college_frame = df[['USN', sub_code, sub_internal]
                   ][df['College Code'] == '1KS']

if college_frame[sub_code].count() == 0:
    quit()

print('For subject : ', sub_code, '\n\n')

uni_avg = round(float(df[sub_code].mean()),2)
coll_avg = round(float(college_frame[sub_code].mean()),2)

uni_iavg = round(float(df[sub_internal].mean()),2)
coll_iavg = round(float(college_frame[sub_internal].mean()),2)

uni_perc = round(float(1-df[[sub_code,]] [df[sub_code] < 40].count()/df[sub_code].count())*100,2)

no_of_coll_students = int(college_frame[[sub_code]].count())
no_pass_coll = int(college_frame[[sub_code]][college_frame[sub_code]>=40].count())
coll_perc = round(float(no_pass_coll/no_of_coll_students)*100,2)

uni_max = int(df[[sub_code]].max())
coll_max = int(college_frame[[sub_code]].max())

print('University Average Score is : ', uni_avg)
print('College Average Score is : ', coll_avg)

print('\nUniversity Average Internals Score is : ', uni_iavg)
print('College Average Internals Score is : ', coll_iavg)

print('\nUniversity result % = ', uni_perc)

print( '\nNo. Of Students attempted the exam : ', no_of_coll_students)
print( 'No. Of Students from passed : ', no_pass_coll)
print( 'Result % = ', coll_perc) 


print('\nMax score in the University : ', uni_max)
print('Max score in the College : ', coll_max)

fig, ax = plt.subplots()
graph = df[sub_code].plot.hist(bins=50, title='University marks distribution Histogram')
# print(fig)
# print(ax)
# plt.boxplot(x=list(df[sub_code]))
# print(list(college_frame[sub_code]))
# print(list(df[sub_code]))
boxframe = pd.concat([college_frame[sub_code],df[sub_code]], axis=1)
boxframe.columns=[ 'College', 'University' ]
boxgraph = boxframe.plot.box(label='Marks', title='Box Graph').get_figure()
print(type(boxgraph))
print(type(fig))
fig.savefig('hists.png')
boxgraph.savefig('boxgr.png')
plt.show()
# print(df.head())
# print(type(df.describe()))

pdf = FPDF()
pdf.add_page()
pdf.set_font('times', 'B', 24)
pdf.write(8, 'Results Analysis for subject '+ sub_code)
pdf.set_font('times', '', 12)
pdf.write(5, '\n\n\n')
pdf.write(5, '\nUniversity Average Score is : ' + str(uni_avg))
pdf.write(5, '\nCollege Average Score is : ' + str(coll_avg))
pdf.write(5, '\n')
pdf.write(5, '\nUniversity Average Internals Score is : ' + str(uni_iavg))
pdf.write(5, '\nCollege Average Internals Score is : ' + str(coll_iavg))
pdf.write(5, '\n')
pdf.write(5, '\nUniversity Pass % = ' + str(uni_perc) + '%')
pdf.write(5, '\n')
pdf.write(5, '\nNo. Of Students attempted the exam : ' + str(no_of_coll_students))
pdf.write(5, '\nNo. Of Students passed : ' + str(no_pass_coll))
pdf.write(5, '\n')
pdf.write(5, '\nResult : ' + str(coll_perc) + '%')
pdf.write(5, '\n')
pdf.write(5, '\nMax score in the University : ' + str(uni_max))
pdf.write(5, '\nMax score in the College : ' + str(coll_max))
pdf.write(5, '\n')
pdf.add_page
pdf.image('boxgr.png', w=200)
pdf.image('hists.png', w=200)
pdf.output(sub_code+'.pdf', 'F')

